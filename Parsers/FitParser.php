<?php

include_once '/var/www/vhosts/toilster/com.toilster.dev/toilster/POC/Classes/config.php';
require_once('/var/www/vhosts/toilster/com.toilster.dev/toilster/POC/Classes/3rdParty/phpFITFileAnalysis/phpFITFileAnalysis.php');

if (DEBUG) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

class FitParser
{
    private $workoutIdentifier = 0; // Unique identifier of workout
    private $fitFileToParse;
    private $pFFA;

    private $gps = array(
        'time_in_s' => array(),
        'latitude' => array(),
        'longitude' => array(),
        'altitude' => array(),
        'km' => array(),
        'heartrate' => array(),
        'rpm' => array(),
        'temp' => array(),
        'power' => array(),
        'groundcontact' => array(),
        'oscillation' => array(),
        'groundcontact_balance' => array(),
        'stroke' => array(),
        'stroketype' => array(),
        'hrv' => array(),
        'smo2_0' => array(),
        'smo2_1' => array(),
        'thb_0' => array(),
        'thb_1' => array(),
    );

    public function __construct($workoutID, $fitFile)
    {
        $this->fitFileToParse = $fitFile;
        $this->workoutIdentifier = $workoutID;

        $options = [
            'fix_data' => ['all'],
            'units' => 'metric',
            'garmin_timestamps' => false
        ];

        try {
            $this->pFFA = new adriangibbonsPOC\phpFITFileAnalysis($fitFile, $options);
        } catch (Exception $e) {
            echo 'Exception has been thrown : ' . $e;
        }
        //
        if (isset($this->pFFA->data_mesgs['file_id']['type']) && $this->pFFA->enumData('file', $this->pFFA->data_mesgs['file_id']['type']) != 'activity')
            throw new Exception('FIT file is not specified as activity.');
        //
    }

    public function getManufacturer()
    {
        $manufacturer = $this->pFFA->manufacturer();
        return isset($manufacturer) ? $manufacturer : "UNKNOWN";
    }

    public function getSport()
    {
        $sport = $this->pFFA->sport();
        return isset($sport) ? $sport : "UNKNOWN";
    }

    public function getProduct()
    {
        $product = $this->pFFA->product();
        return isset($product) ? $product : "UNKNOWN";
    }

    public function getMaxSpeed()
    {
        $maxSpeed = max($this->pFFA->data_mesgs['record']['speed']);
        return isset($maxSpeed) ? $maxSpeed : "UNKNOWN";
    }

    public function getAvgSpeed()
    {
        $avgSpeed = array_sum($this->pFFA->data_mesgs['record']['speed']) / count($this->pFFA->data_mesgs['record']['speed']);
        return isset($avgSpeed) ? $avgSpeed : "UNKNOWN";
    }

    public function getMinSpeed()
    {
        $minSpeed = min($this->pFFA->data_mesgs['record']['speed']);
        return isset($minSpeed) ? $minSpeed : "UNKNOWN";
    }

    public function getMaxHeartRate()
    {
        $maxHR = $this->pFFA->data_mesgs['session']['max_heart_rate'];
        return isset($maxHR) ? $maxHR : "UNKNOWN";
    }

    public function getAvgHeartRate()
    {
        $avgHR = $this->pFFA->data_mesgs['session']['average_heart_rate'];
        return isset($avgHR) ? $avgHR : "UNKNOWN";
    }

    public function getTotalDistance()
    {
        $totalDistance = round($this->pFFA->data_mesgs['session']['total_distance'], 3);
        return isset($totalDistance) ? $totalDistance : "UNKNOWN";
    }

    public function getTotalCalories()
    {
        $totalCalories = $this->pFFA->data_mesgs['session']['total_calories'];
        return isset($totalCalories) ? $totalCalories : "UNKNOWN";
    }

    public function printRawGPSArray()
    {
        //Parse firts
        $this->tryToParse();
        //
//        return print_r($this->gps['latitude']); // just time in seconds as an array
        return print_r($this->gps); // just time in seconds as an array
// NOT WORKING BELOW
/*
        foreach ($this->gps as $key) {
            foreach ($key as $record)
            {
                echo $record['latitude'];
            }
        }
*/
    }

    // test part
    private function tryToParse()
    {
        /* try to map all internal fields to FIT fields */
        foreach (array_keys($this->gps) as $key) {
            //$fitkey = $this->mapGPStoFITkey($key, $this->isSwim); // TODO swim
            $fitkey = $this->mapGPStoFITkey($key, false);
            $fittype = $this->mapFITtoType($fitkey);
            if (isset($this->pFFA->data_mesgs[$fittype][$fitkey]))
                $this->gps[$key] = array_values($this->pFFA->data_mesgs[$fittype][$fitkey]);
        }
        // AFter that I think I can parse it as normally
    }

    // Try to map FIT arrays to internal GPS arrays (keys)
    private function mapGPStoFITkey($String, $swim = false)
    {
        switch ($String) {
            case 'time_in_s':
                return 'timestamp';
            case 'latitude':
                return 'position_lat';
            case 'longitude':
                return 'position_long';
            case 'km':
                return 'distance';
            case 'stroke':
                return 'total_strokes';
            case 'stroketype':
                return 'swim_stroke';
            case 'rpm':
                return $swim ? 'avg_swimming_cadence' : 'cadence';
            case 'heartrate':
                return 'heart_rate';
            case 'temp':
                return 'temperature';
        }

        return $String;
    }


    //Try to map FIT arrays to FIT internal type
    private function mapFITtoType($String)
    {
        switch ($String) {
            case 'stroke':
            case 'stroketype':
            case 'avg_swimming_cadence':
                return 'length';
                break;
        }

        return 'record';
    }
}
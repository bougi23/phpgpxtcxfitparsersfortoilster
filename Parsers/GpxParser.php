<?php

include_once '../config.php';

if (DEBUG) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

class GpxParser
{
    private $DESTINATION_PATH = DIR_BASE . 'toilster/POC/Data/Processed/GPX/';
    //
    private $gpxFileToParse; // File to be parsed i.e. workout.gpx
    private $parsedFile; // Parsed File into the JSON
    private $gpxFilePath;
    private $isParsed = false; // Indicates if file is already parse and we have parsedJson
    private $maxElevation = 0; // Stores max ELEVATION point
    private $minElevation = 10000; // Stores min ELEVATION point
    private $maxSpeed = 0; // Stores max SPEED point
    private $minSpeed = 10000; // Stores min SPEED point
    private $maxHeartRate = 0; // Stores max HR point
    private $minHeartRate = 10000; // Stores min HR point
    private $heartRateSamples = array(); // An array of heart rate samples
    private $maxCadence = 0; // Stores max CADENCE point
    private $minCadence = 10000; // Stores min CADENCE point
    private $maxTemperature = 0; // Stores max TEMP point
    private $minTemperature = 10000; // Stores min TEMP point
    private $maxAltitude = 0; // Stores max ALTITUDE point
    private $minAltitude = 10000; // Stores min ALTITUDE point
    private $totalDistance = 0; // Stores total distance incremented always between two points
    private $totalDuration = 0; // Stores total duration time of workout endTimestamp - startTimestamp
    private $workoutIdentifier = 0; // Unique identifier of workout
    private $workoutType = 0;
    private $isMapAvailable = false; // checked only on creation !!!
    private $ownerUID;
    // TODO LAPS
    private $lapTimeSamples = array(); // An array of lap and time samples
    private $numberOfLaps = 1;
    private $lastKnownTime = 0;
    // Extensions from file existence
    private $isHeartRateExist = false; // Indicates if heart rate exist in GPX file
    private $isCadenceExist = false; // Indicates if cadence exist in GPX file
    private $isTemperatureExist = false; // Indicates if temperature exist in GPX file
    private $isAltitudeExist = false; // indicates if altitude exist in GPX file
    // Temporary storage for previous lat and lon and timestamp
    private $previousLat = 0;
    private $previousLon = 0;
    private $previousTimestamp = 0;
    private $previousHR = 0;
//
    private $isFileProcessed = false;

    public function __construct($workoutID, $gpxFile, $ownerUID)
    {
        $this->workoutIdentifier = $workoutID;
        $this->gpxFileToParse = $gpxFile;
        $this->gpxFilePath = $this->DESTINATION_PATH . $this->workoutIdentifier . '_GPX.gpx';
        //
        isset($ownerUID) ? $this->ownerUID = $ownerUID : $this->ownerUID = 'UNKNOWN';
        //
        try {
            $this->saveGPXFileToDisk($this->gpxFilePath, file_get_contents($gpxFile));
            $this->parseGpxFile($this->gpxFileToParse);
        } catch (Exception $e) {
            echo 'Exception has been thrown : ' . $e;
        }
    }

    private function saveGPXFileToDisk($filename, $data)
    {
        $saveFileToDestination = file_put_contents($filename, $data);
        if (!$saveFileToDestination)
            throw new Exception('Unable to save GPX file: ' . $filename);
    }

    private function parseGpxFile($fileToParse)
    {
        $response = array();

        $xml = simplexml_load_file($fileToParse);

        $trackPoints = $xml->trk;
        $geoPoints = $xml->trk->trkseg->trkpt;

        // Get the sport/workout type
        foreach ($trackPoints as $p) {
            $this->workoutType = isset($p->type) ? $p->type : 'UNKNOWN';
        }

        foreach ($geoPoints as $p) {
            $attributes = $p->attributes();
            $date = preg_replace(array('/T/', '/Z/'), array(' ', ''), $p->time);
            //
            if ($this->checkValueExistence($attributes->lat) && $this->checkValueExistence($attributes->lon)) {
                $this->isMapAvailable = true;
            }
            if ($this->checkValueContains('e', strtolower($attributes->lat)) || $this->checkValueContains('e', strtolower($attributes->lon))) {
                continue;
            }
            if ($attributes->lat == 0 || $attributes->lon == 0) {
                continue;
            }
            //
            $currentTime = (string)strtotime($date);
            $currentLat = ($this->checkValueExistence($attributes->lat)) ? (float)$attributes->lat : $this->previousLat;
            $currentLon = ($this->checkValueExistence($attributes->lon)) ? (float)$attributes->lon : $this->previousLon;
            $currentEle = ($this->checkValueExistence((float)$p->ele)) ? (float)$p->ele : '0';
            //
            $currentDistance = $this->getDistanceBetweenCoords((float)$this->previousLat, (float)$this->previousLon, $currentLat, $currentLon);
            $currentSpeed = $this->getSpeedBetweenCoords($this->previousLat, $this->previousLon, $currentLat, $currentLon, $this->previousTimestamp, $currentTime);
            // Extensions variable
            $currentHR = 0;
            $currentCadence = 0;
            $currentTemp = 0;
            $currentAltitude = 0;

            // check if file contains EXTENSIONS
            if (isset($p->extensions)) {
                foreach ($this->getExtensionNodesOf($p->extensions) as $extensionNode) {
                    //
                    if ($extensionNode instanceof \SimpleXMLElement) {
                        // HR
                        if (isset($extensionNode->hr)) {
                            $this->isHeartRateExist = true;
                            $currentHR = (int)$extensionNode->hr;
                            array_push($this->heartRateSamples, $currentHR);
                        }
                        // CADENCE
                        if (isset($extensionNode->cad)) {
                            $this->isCadenceExist = true;
                            $currentCadence = (int)$extensionNode->cad;
                        } elseif (isset($extensionNode->cadence)) {
                            $this->isCadenceExist = true;
                            $currentCadence = (int)$extensionNode->cadence;
                        }
                        // TEMP
                        if (isset($extensionNode->temp)) {
                            $this->isTemperatureExist = true;
                            $currentTemp = (int)$extensionNode->temp;
                        } elseif (isset($extensionNode->atemp)) {
                            $this->isTemperatureExist = true;
                            $currentTemp = (int)$extensionNode->atemp;
                        }
                        // ALTITUDE
                        // TODO add altitute like it is in https://github.com/Runalyze/Runalyze/commit/7e2458c5071f1e7356aa52cd4f655df6b2b62b4a
                        // TODO collumns in database
                        if (isset($extensionNode->altitude)) {
                            $this->isAltitudeExist = true;
                            $currentAltitude = (int)$extensionNode->altitude;
                        }
                    }
                }
            }

            //
            $geoPoint = (object)array(
                't' => $currentTime,
                'lat' => $currentLat,
                'lon' => $currentLon,
                'ele' => round($currentEle),
                'speed' => $currentSpeed,
                'distance' => round($this->totalDistance) + round($currentDistance),
                'hr' => $currentHR
            );

            // set the MIN or MAX elevation point
            $this->increaseOrDecreaseElevation($currentEle);
            // set the MIN or MAX speed point
            $this->increaseOrDecreaseSpeed($currentSpeed);
            // set the MIN or MAX heart rate point {if exists any}
            if ($this->isHeartRateExist)
                $this->increaseOrDecreaseHeartRate($currentHR);
            // set the MIN or MAX Cadence point (if exists any)
            if ($this->isCadenceExist)
                $this->increaseOrDecreaseCadence($currentCadence);
            // set the MIN or MAX Temperature point (if exists any)
            if ($this->isTemperatureExist)
                $this->increaseOrDecreaseTemperature($currentTemp);
            // the MIN or MAX Altitude point (if exists any
            if ($this->isAltitudeExist)
                $this->increaseOrDecreaseAltitude($currentAltitude);

            // If we have lat and lon and timestamp we can save them as previous
            $this->previousLat = $currentLat;
            $this->previousLon = $currentLon;
            $this->previousTimestamp = $currentTime;
            // Increase total distance
            $this->totalDistance = $this->totalDistance + $currentDistance;

            // TODO Complexity -> standalone method
            // TODO last lap
            if ($this->totalDistance / 1000 >= (1 * $this->numberOfLaps)) {
                // Only for first lap
                if ($this->numberOfLaps == 1)
                    $this->lastKnownTime = $response[0]->t;
                // We have lap -> Write data to array
                // TODO add fastest/slowest in percents
                $lapTimePoint = (object)array(
                    'lap' => $this->numberOfLaps, // lap #
                    'elapsedTime' => $this->convertSecondsToMMSS($currentTime - $this->lastKnownTime), // time per lap
                    'percentage' => $currentTime - $this->lastKnownTime
                );

                // Add point info to JSON array
                array_push($this->lapTimeSamples, $lapTimePoint);
                // Increase lap number
                $this->numberOfLaps++;
                // Increase last known time by current time
                $this->lastKnownTime = $currentTime;
            }

            // Add point info to JSON array
            array_push($response, $geoPoint);
        }

        $this->totalDuration = $response[count($response) - 1]->t - $response[0]->t;

        // file is parsed
        $this->isParsed = true;
        // Saving parsed file to variable as JSON
        $this->parsedFile = json_encode($response);

        // Saving workout to database
        if ($this->saveUserWorkoutToDB()) {
            $this->isFileProcessed = true;
        } else {
            throw new Exception('Unable to save workout id: ' . $this->workoutIdentifier . ' to database.');
        }
    }

    private function checkValueExistence($value)
    {
        return (isset($value) && !empty($value));
    }

    private function checkValueContains($needle, $haystack)
    {
        // returns true if $needle is a substring of $haystack
        return strpos($haystack, $needle) !== false;
    }

    private function getDistanceBetweenCoords($lat1, $lon1, $lat2, $lon2)
    {
        // http://www.geodatasource.com/developers/php
        $theta = $lon1 - $lon2;
        if ($theta == 0)
            $theta = 0.00001; // HACK automaticky nasimulujeme pohyb kdyby lon1 a lon2 byly stejne tj. minimalni pohyb

        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $result = $dist * 60 * 1.1515;

        $result = ($result * 1609.344);
        $result = ($result > 100) ? 0 : $result; // HACK nekdy je spatna vzdalenost .. vetsi nez 100 metru na dvou bodech nikdy nebude

        return $result;
    }

    private function getSpeedBetweenCoords($lat1, $lon1, $lat2, $lon2, $timestamp1, $timestamp2)
    {
        // TODO add to helpe class
        $time = $timestamp2 - $timestamp1;
        if ($time == 0 || $time < 0)
            $time = 1; // HACK automaticky nasimulujeme pohyb kdyby lon1 a lon2 byly stejne tj. minimalni pohyb

        $theta = $lon1 - $lon2;
        if ($theta == 0)
            $theta = 0.00001; // HACK automaticky nasimulujeme pohyb kdyby lon1 a lon2 byly stejne tj. minimalni pohyb

        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $result = $dist * 60 * 1.1515;

        $result = ($result * 1.609344);
        $result = ($result > 100) ? 0 : $result; // HACK nekdy je spatna vzdalenost .. vetsi nez 100 metru na dvou bodech nikdy nebude

        $speedInMPS = $result / $time;
        $speedInKPH = ($speedInMPS * 3600);

        return $speedInKPH;

    }

    private function getExtensionNodesOf(SimpleXMLElement $parentNode)
    {
        $childNodes = [];

        foreach (['gpxtpx', 'ns3', 'ns2'] as $namespace) {
            if (
                count($parentNode->children($namespace, true)) > 0 &&
                isset($parentNode->children($namespace, true)->TrackPointExtension) &&
                count($parentNode->children($namespace, true)->TrackPointExtension) > 0
            ) {
                $childNodes[] = $parentNode->children($namespace, true)->TrackPointExtension->children($namespace, true);
            }
        }

        if (count($parentNode->children('gpxdata', true)) > 0) {
            $childNodes[] = $parentNode->children('gpxdata', true);
        }

        return $childNodes;
    }

    private function increaseOrDecreaseElevation($currentElevationPoint)
    {
        // Increase
        if ($currentElevationPoint > $this->maxElevation && !empty($currentElevationPoint)) {
            $this->maxElevation = $currentElevationPoint;
        }
        // Decrease
        if ($currentElevationPoint < $this->minElevation && !empty($currentElevationPoint)) {
            $this->minElevation = $currentElevationPoint;
        }
    }

    private function increaseOrDecreaseSpeed($currentSpeedPoint)
    {
        // Increase
        if ($currentSpeedPoint > $this->maxSpeed && !empty($currentSpeedPoint)) {
            $this->maxSpeed = $currentSpeedPoint;
        }
        // Decrease
        if ($currentSpeedPoint < $this->minSpeed && !empty($currentSpeedPoint)) {
            $this->minSpeed = $currentSpeedPoint;
        }
    }

    private function increaseOrDecreaseHeartRate($currentHeartRatePoint)
    {
        // Increase
        if ($currentHeartRatePoint > $this->maxHeartRate && !empty($currentHeartRatePoint)) {
            $this->maxHeartRate = $currentHeartRatePoint;
        }
        // Decrease
        if ($currentHeartRatePoint < $this->minHeartRate && !empty($currentHeartRatePoint)) {
            $this->minHeartRate = $currentHeartRatePoint;
        }
    }

    private function increaseOrDecreaseCadence($currentCadencePoint)
    {
        // Increase
        if ($currentCadencePoint > $this->maxCadence && !empty($currentCadencePoint)) {
            $this->maxCadence = $currentCadencePoint;
        }
        // Decrease
        if ($currentCadencePoint < $this->minCadence && !empty($currentCadencePoint)) {
            $this->minCadence = $currentCadencePoint;
        }
    }

    private function increaseOrDecreaseTemperature($currentTemperaturePoint)
    {
        // Increase
        if ($currentTemperaturePoint > $this->maxTemperature && !empty($currentTemperaturePoint)) {
            $this->maxTemperature = $currentTemperaturePoint;
        }
        // Decrease
        if ($currentTemperaturePoint < $this->minTemperature && !empty($currentTemperaturePoint)) {
            $this->minTemperature = $currentTemperaturePoint;
        }
    }

    private function increaseOrDecreaseAltitude($currentAltitudePoint)
    {
        // Increase
        if ($currentAltitudePoint > $this->maxAltitude && !empty($currentAltitudePoint)) {
            $this->maxAltitude = $currentAltitudePoint;
        }
        // Decrease
        if ($currentAltitudePoint < $this->minAltitude && !empty($currentAltitudePoint)) {
            $this->minAltitude = $currentAltitudePoint;
        }
    }

    private function convertSecondsToMMSS($time)
    {
        return gmdate("i:s", $time);
    }

    private function saveUserWorkoutToDB()
    {
        // Here write data to sql
        $db = getDB();
        $stmt = $db->prepare("INSERT INTO UserWorkout (fileName, fileExtension, path, jsonData, maxElevation, minElevation, totalDistance, totalDuration, maxSpeed, minSpeed, maxHeartRate, minHeartRate, heartRateSamples, maxCadence, minCadence, maxTemperature, minTemperature, maxAltitude, minAltitude, lapTimeSamples, isPreviewCreated, workoutType, ownerID, workoutID)
                                    VALUES (:fileName, :fileExtension, :path, :jsonData, :maxElevation, :minElevation, :totalDistance, :totalDuration, :maxSpeed, :minSpeed, :maxHeartRate, :minHeartRate, :heartRateSamples, :maxCadence, :minCadence, :maxTemperature, :minTemperature, :maxAltitude, :minAltitude, :lapTimeSamples, :isPreviewCreated, :workoutType, :ownerID, :workoutID)");
        $stmt->bindValue(':fileName', $this->getFileName(), \PDO::PARAM_STR);
        $stmt->bindValue(':fileExtension', $this->getFileExtension(), \PDO::PARAM_STR);
        $stmt->bindValue(':path', $this->gpxFilePath, \PDO::PARAM_STR);
        $stmt->bindValue(':jsonData', $this->parsedFile, \PDO::PARAM_STR);
        // not sure about params
        $stmt->bindValue(':maxElevation', $this->maxElevation, \PDO::PARAM_STR); // not sure if string type
        $stmt->bindValue(':minElevation', $this->minElevation, \PDO::PARAM_STR); // not sure if string type
        $stmt->bindValue(':totalDistance', $this->totalDistance, \PDO::PARAM_STR); // not sure if string type
        $stmt->bindValue(':totalDuration', $this->totalDuration, \PDO::PARAM_STR); // not sure if string type
        $stmt->bindValue(':maxSpeed', $this->maxSpeed, \PDO::PARAM_STR); // not sure if string type
        $stmt->bindValue(':minSpeed', $this->minSpeed, \PDO::PARAM_STR); // not sure if string type
        //
        if ($this->isHeartRateExist && $this->maxHeartRate != 0 && $this->minHeartRate != 10000) {
            $stmt->bindValue(':maxHeartRate', $this->maxHeartRate, \PDO::PARAM_STR); // not sure if string type
            $stmt->bindValue(':minHeartRate', $this->minHeartRate, \PDO::PARAM_STR); // not sure if string type
            $stmt->bindValue(':heartRateSamples', json_encode($this->heartRateSamples), \PDO::PARAM_STR); // not sure if string type
        } else {
            $stmt->bindValue(':maxHeartRate', null, \PDO::PARAM_STR); // not sure if string type
            $stmt->bindValue(':minHeartRate', null, \PDO::PARAM_STR); // not sure if string type
            $stmt->bindValue(':heartRateSamples', null, \PDO::PARAM_STR); // not sure if string type
        }
        //
        if ($this->isCadenceExist && $this->maxCadence != 0 && $this->minCadence != 10000) {
            $stmt->bindValue(':maxCadence', $this->maxCadence, \PDO::PARAM_STR); // not sure if string type
            $stmt->bindValue(':minCadence', $this->minCadence, \PDO::PARAM_STR); // not sure if string type
        } else {
            $stmt->bindValue(':maxCadence', null, \PDO::PARAM_STR); // not sure if string type
            $stmt->bindValue(':minCadence', null, \PDO::PARAM_STR); // not sure if string type
        }
        //
        if ($this->isTemperatureExist && $this->maxTemperature != 0 && $this->minTemperature != 10000) {
            $stmt->bindValue(':maxTemperature', $this->maxTemperature, \PDO::PARAM_STR); // not sure if string type
            $stmt->bindValue(':minTemperature', $this->minTemperature, \PDO::PARAM_STR); // not sure if string type
        } else {
            $stmt->bindValue(':maxTemperature', null, \PDO::PARAM_STR); // not sure if string type
            $stmt->bindValue(':minTemperature', null, \PDO::PARAM_STR); // not sure if string type
        }
        // TODO add this section to TCX -> and to GETTERS as well ! workout.PHP
        if ($this->isAltitudeExist && $this->maxAltitude != 0 && $this->minAltitude != 10000) {
            $stmt->bindValue(':maxAltitude', $this->maxAltitude, \PDO::PARAM_STR); // not sure if string type
            $stmt->bindValue(':minAltitude', $this->minAltitude, \PDO::PARAM_STR); // not sure if string type
        } else {
            $stmt->bindValue(':maxAltitude', null, \PDO::PARAM_STR); // not sure if string type
            $stmt->bindValue(':minAltitude', null, \PDO::PARAM_STR); // not sure if string type
        }
        $stmt->bindValue(':lapTimeSamples', json_encode($this->lapTimeSamples), \PDO::PARAM_STR); // not sure if string type
        $stmt->bindValue(':isPreviewCreated', '0', \PDO::PARAM_STR); // default is false
        $stmt->bindValue(':workoutType', $this->workoutType, \PDO::PARAM_STR); // not sure if string type
        $stmt->bindValue(':ownerID', $this->ownerUID, \PDO::PARAM_STR); // not sure if string type
        $stmt->bindValue(':workoutID', $this->workoutIdentifier, \PDO::PARAM_INT); // not sure if string type
        //
        $db = null;
        return $stmt->execute(); // if executed true else false
    }

    private function getFileName()
    {
        if ($this->isParsed) {
            $path_parts = pathinfo($this->gpxFileToParse);
            return $path_parts['basename'];
        } else {
            return;
        }
    }

    private function getFileExtension()
    {
        if ($this->isParsed && !empty($this->gpxFileToParse)) {
            $path_parts = pathinfo($this->gpxFileToParse);
            return $path_parts['extension'];
        } else {
            return;
        }
    }

    public function getParsingResult()
    {
        return $this->isFileProcessed;
    }
}

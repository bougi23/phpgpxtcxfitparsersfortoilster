<?php

include_once '../config.php';

if (DEBUG) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

include_once 'GpxParser.php';
include_once 'TcxParser.php';
include_once 'FitParser.php';

class CommonParser
{
    private $workoutIdentifier = 0;
    private $isParsed = false; // Status of parsing process
    private $parser = null;

    public function __construct($fileToParse)
    {
        $this->fileToParse($fileToParse);
    }

    private function fileToParse($fileToParse)
    {
        // Set WorkoutID
        $this->setWorkoutID();
        // getFileType and create an instance of that object
        switch ($this->getFileType($fileToParse)) {
            case "gpx":
                // Parsing with GPX
                $this->gpxParsing($fileToParse);
                break;
            case "tcx":
                // Parsing with TCX
                $this->tcxParsing($fileToParse);
                break;
            case "fit":
                // Parsing with FIT
                $this->fitParsing($fileToParse);
                break;
            case "xml":
                // Parsing with XML -> there are few types so maybe some other condition needs to be checked
                echo "Parsing with XML";
                break;
            default:
                echo "This is unknown FILE type we cannot parse it right now.";
                $this->isParsed = false;
                $this->workoutIdentifier = 0;
        }
    }

    private function setWorkoutID()
    {
        $workoutID = md5(time());
        $this->workoutIdentifier = $workoutID; // Saving in class object
        return $workoutID;
    }

    private function getFileType($fileToParse)
    {
        $path_parts = pathinfo($fileToParse);
        return strtolower($path_parts['extension']);
    }

    private function gpxParsing($fileToParse)
    {
        $this->parser = new GpxParser($this->workoutIdentifier, $fileToParse, null);
        $this->isParsed = $this->parser->getParsingResult();
    }

    private function tcxParsing($fileToParse)
    {
        $this->parser = new TcxParser($this->workoutIdentifier, $fileToParse, null);
        $this->isParsed = $this->parser->getParsingResult();
    }

    private function fitParsing($fileToParse)
    {
        $this->parser = new FitParser($this->workoutIdentifier, $fileToParse);
        //$this->isParsed = $this->parser->getParsingResult(); // TODO get result of parsing process
    }

    public function getWorkoutID()
    {
        return $this->workoutIdentifier;
    }

    public function getLityURL()
    {
        return "<a href=\"http://dev.toilster.com/toilster/POC/index.php?workoutID=" . $this->workoutIdentifier . "\" data-lity>Workout Preview</a>";
    }

    public function getIsParsed()
    {
        return $this->isParsed;
    }
}

// Getting file from POST
if (isset($_FILES["file"]["name"])) {
    $name = $_FILES["file"]["name"];
    $tmp_name = $_FILES['file']['tmp_name'];
    $error = $_FILES['file']['error'];
    $uid = md5(time());
    //
    if (!empty($name)) {
        $location = DIR_BASE . 'toilster/POC/Data/Processed/Uploads/';

        if (move_uploaded_file($tmp_name, $location . $uid . $name)) {
            //echo 'Uploaded';
            $parser = new CommonParser($location . $uid . $name);
            if ($parser->getIsParsed()) {
                echo $parser->getLityURL();
            }
        } else {
            throw new Exception('Unable to save or upload file: ' . $name);
        }
    } else {
        echo 'please choose a file';
    }
}
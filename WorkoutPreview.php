<?php

include_once 'config.php';
include_once 'Workout.php';
include_once '3rdParty/google_map_polyline_encoding_tool/Polyline.php';
include_once '3rdParty/simpleImage/SimpleImage.php';

if (DEBUG) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

class WorkoutPreview
{
    // TODO get from database isMapAvailable boolean
    private $jsonData; // Raw JSON data from database
    private $coordinates = array(); // Coordinates extracted from raw JSON data
    private $reducedCoordinates = array(); // Coordinates reduce by algorithm
    private $reducedEncodedCoordinates = array(); // Reduced and encoded coordinates by encoder // TODO
    private $workout; // Workout object generated based on workoutID passed by constructor
    private $workoutID; // Workout ID
    private $isPreviewCreated; // Boolean from database -> default is false -> when workout preview image is created it is UPDATED to true
    private $workoutPreviewFileName; // The name of the final workout preview file

    private $DESTINATION_PATH = DIR_BASE . 'toilster/POC/Data/imagePreviewTemplate/';
    private $FONT_PATH = DIR_BASE . 'toilster/POC/fonts/';

    public function __construct($workoutID)
    {
        if (isset($workoutID) && $workoutID != '0') {
            $this->workoutID = $workoutID;
            $this->workout = new Workout($workoutID);
            $this->jsonData = json_decode($this->workout->getJSON());
            $this->isPreviewCreated = (bool)$this->workout->getIsPreviewCreated();
            //
            $this->parseCoordinates();
            //
            $this->tryToCreateWorkoutPreviewImage();
        } else {
            echo 'Workout ID does not exist.';
        }
    }

    private function parseCoordinates()
    {
        foreach ($this->jsonData as $latlon) {
            $newData = (object)array(
                'lat' => $latlon->lat,
                'lon' => $latlon->lon
            );
            array_push($this->coordinates, $newData);
        }
    }

    private function tryToCreateWorkoutPreviewImage()
    {
        // TODO CHECK ONLY if preview is created or not
        // TODO ADD check on isMapAvailable also
        if (!$this->isPreviewCreated) {
            $this->createWorkoutPreviewWithMap();
        } else {
            //$this->createWorkoutPreviewWithoutMap();
        }
    }

    private function createWorkoutPreviewWithMap()
    {
        $mapPreview = $this->DESTINATION_PATH . 'workout_map_preview_' . $this->workoutID . '.jpg';
        $workoutInfoPreview = $this->DESTINATION_PATH . 'workout_info_preview_' . $this->workoutID . '.jpg';
        $finalPreview = $this->DESTINATION_PATH . 'workout_final_preview_' . $this->workoutID . '.jpg';
        //
        try {
            // Create Info picture by populating Template_Blank.jpg with workoutdata
            $this->createWorkoutPreviewInfoPart();
            // Create Map picture
            $this->createWorkoutPreviewMapPart();
            // Merge this two images together
            $this->merge(
                $mapPreview, // first
                $workoutInfoPreview, // second
                $finalPreview // result
            );
            // Delete files we dont need anymore
            if (file_exists($mapPreview) && file_exists($workoutInfoPreview)) {
                unlink($mapPreview);
                unlink($workoutInfoPreview);
            }
            // Update DB
            $this->updateDatabasePreviewIsCreated();
            // Save file Name
            $this->workoutPreviewFileName = $finalPreview;
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    private function createWorkoutPreviewInfoPart()
    {
        $workoutInfoPreview = $this->DESTINATION_PATH . 'workout_info_preview_' . $this->workoutID . '.jpg';
        // Create Info picture by populating Template_Blank.jpg with workoutdata
        // ./Data/imagePreviewTemplate/workout_info_preview_WORKOUTID.jpg
        $img = new abeautifulsite\SimpleImage($this->DESTINATION_PATH . 'template_blank_new.jpg');
        // TODO improve coords of text below
        $img->text($this->workout->getTotalDistanceInKiloMeters(), $this->FONT_PATH . 'delicious.ttf', 32, '#FFFFFF', 'top left', 20, 50);
        $img->text($this->workout->getTotalDuration(), $this->FONT_PATH . 'delicious.ttf', 32, '#FFFFFF', 'top left', 280, 50);
        $img->text($this->workout->getAvaragePace(), $this->FONT_PATH . 'delicious.ttf', 32, '#FFFFFF', 'top left', 520, 40);
        $img->text($this->workout->getBurnedCalories(), $this->FONT_PATH . 'delicious.ttf', 32, '#FFFFFF', 'top left', 20, 120);
        $img->text($this->workout->getMinElevation(), $this->FONT_PATH . 'delicious.ttf', 32, '#FFFFFF', 'top left', 280, 120);
        $img->text($this->workout->getMaxElevation(), $this->FONT_PATH . 'delicious.ttf', 32, '#FFFFFF', 'top left', 520, 120);
        //
        $img->save($workoutInfoPreview);
    }

    private function createWorkoutPreviewMapPart()
    {
        $mapPreview = $this->DESTINATION_PATH . 'workout_map_preview_' . $this->workoutID . '.jpg';
        // Create Map picture
        // ./Data/imagePreviewTemplate/workout_map_preview_WORKOUTID.jpg
        $encodedPolylines = $this->encodeCoords();
        // TODO polyline in color as it is in preview
        $googleStaticMapImage = file_get_contents('http://maps.googleapis.com/maps/api/staticmap?size=640x250&maptype=terrain&path=enc:' . $encodedPolylines);
        $fp = fopen($mapPreview, 'w+');

        fputs($fp, $googleStaticMapImage);
        fclose($fp);
        unset($googleStaticMapImage);
    }

    private function encodeCoords()
    {
        // https://github.com/adriangibbons/php-fit-file-analysis/blob/master/demo/mountain-biking.php INSPIRACEk,
        foreach ($this->coordinates as $latlon) {
            $newData = array(
                array($latlon->lat, $latlon->lon)
            );
            array_push($this->reducedCoordinates, $newData);
        }

        return Polyline::encode($this->reducedCoordinates);
    }

    private function merge($filename_x, $filename_y, $filename_result)
    {
        // Get dimensions for specified images
        list($width_x, $height_x) = getimagesize($filename_x);
        list($width_y, $height_y) = getimagesize($filename_y);
        // Create new image with desired dimensions
        $image = imagecreatetruecolor($width_x, $height_x + $height_y);
        // Load images and then copy to destination image
        $image_x = imagecreatefrompng($filename_x);
        $image_y = imagecreatefromjpeg($filename_y);

        imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);
        imagecopy($image, $image_y, 0, $height_x, 0, 0, $width_y, $height_y);
        // Save the resulting image to disk (as JPEG)
        imagejpeg($image, $filename_result);
        // Clean up
        imagedestroy($image);
        imagedestroy($image_x);
        imagedestroy($image_y);
    }

    private function updateDatabasePreviewIsCreated()
    {
        $db = getDB();
        // UPDATE UserWorkout set isPreviewCreated=1 WHERE workoutID='1033128036e50731160a3768f0a5fc34'
        $stmt = $db->prepare("UPDATE UserWorkout SET isPreviewCreated=1 WHERE workoutID=:workoutID");
        $stmt->bindParam("workoutID", $this->workoutID, PDO::PARAM_STR);
        $stmt->execute();
        $db = null;
    }

    public function getWorkoutPreviewFileName()
    {
        return $this->workoutPreviewFileName;
    }

    public function getCoordinatesTest()
    {
        return json_encode($this->coordinates);
    }

    public function getJSONdataTest()
    {
        return json_encode($this->jsonData);
    }
}
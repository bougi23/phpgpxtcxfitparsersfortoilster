<?php

include_once 'config.php';

if (DEBUG) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

// Class to get workout object from database for preview in template
class Workout
{
    private $workoutID = 0;
    private $parsedFile; // Parsed File into the JSON
    private $maxElevation = 0; // Stores max ELEVATION point
    private $minElevation = 10000; // Stores min ELEVATION point
    private $maxSpeed = 0; // Stores max SPEED point
    private $minSpeed = 10000; // Stores min SPEED point
    private $maxHeartRate = 0; // Stores max HR point
    private $minHeartRate = 10000; // Stores min HR point
    private $heartRateSamples = array(); // An array of heart rate samples
    private $maxCadence = 0; // Stores max CADENCE point
    private $minCadence = 10000; // Stores min CADENCE point
    private $maxTemperature = 0; // Stores max TEMP point
    private $minTemperature = 10000; // Stores min TEMP point
    private $totalDistance = 0; // Stores total distance incremented always between two points
    private $totalDuration = 0; // Stores total duration time of workout endTimestamp - startTimestamp
    private $workoutIdentifier = 0; // Unique identifier of workout
    private $workoutType = 0;
    private $lapTimeSamples = array(); // An array of lap and time samples
    private $fileName;
    private $filePath;
    private $weight = 80;
    // Boolean values
    private $isParsed = false; // Indicates if file is already parse and we have parsedJson
    private $isMapAvailable = false; // checked only on creation !!!
    private $isHeartRateExist = false; // Indicates if heart rate exist in GPX file
    private $isCadenceExist = false; // Indicates if cadence exist in GPX file
    private $isTemperatureExist = false;
    private $isPreviewCreated = false;

    public function __construct($workoutID)
    {
        if (isset($workoutID) && $workoutID != '0') {
            $this->workoutID = $workoutID;
            $this->getWorkoutDataFromDatabase($workoutID);
        } else {
            echo 'Workout ID does not exist.';
        }
    }

    // This function will get and parse raw data from database
    private function getWorkoutDataFromDatabase($workoutID)
    {
        $db = getDB();
        $stmt = $db->prepare("SELECT * FROM UserWorkout WHERE workoutID=:workout_id");
        $stmt->bindValue(':workout_id', $workoutID, PDO::PARAM_STR);
        $queryStatus = $stmt->execute(); // if query is good
        $data = $stmt->fetchAll();
        $db = null;
        //
        $result = "";
        if ($queryStatus && $stmt->rowCount() > 0) {
            //
            foreach ($data as $row) {
                $this->maxElevation = $row['maxElevation'];
                $this->minElevation = $row['minElevation'];
                $this->totalDistance = $row['totalDistance'];
                $this->totalDuration = $row['totalDuration'];
                //
                if (isset($row['maxSpeed']) && isset($row['minSpeed'])) {
                    $this->maxSpeed = $row['maxSpeed'];
                    $this->minSpeed = $row['minSpeed'];
                }
                //
                if (isset($row['maxHeartRate']) && isset($row['minHeartRate'])) {
                    $this->isHeartRateExist = true;
                    $this->maxHeartRate = $row['maxHeartRate']; // is exist
                    $this->minHeartRate = $row['minHeartRate']; // is exist
                    $this->heartRateSamples = json_decode($row['heartRateSamples'], true); // exist as well
                }
                //
                if (isset($row['maxCadence']) && isset($row['minCadence'])) {
                    $this->isCadenceExist = true;
                    $this->maxCadence = $row['maxCadence']; // is exist
                    $this->minCadence = $row['minCadence']; // is exist
                }
                //
                if (isset($row['maxTemperature']) && isset($row['minTemperature'])) {
                    $this->isTemperatureExist = true;
                    $this->maxTemperature = $row['maxCadence']; // is exist
                    $this->minTemperature = $row['minCadence']; // is exist
                }
                //
                $this->lapTimeSamples = json_decode($row['lapTimeSamples'], true); // exist as well
                $this->workoutType = $row['workoutType'];
                //
                if (isset($row['fileName'])) {
                    $this->fileName = $row['fileName'];
                }
                //
                if (isset($row['path'])) {
                    $this->filePath = $row['path'];
                }
                //
                if (isset($row['workoutID'])) {
                    $this->workoutIdentifier = $row['workoutID'];
                }
                $this->isPreviewCreated = (bool)$row['isPreviewCreated'];

                $result = $row['jsonData'];
            }
            // Setting isParsed
            $this->isParsed = true;
        } else {
            // Setting isParsed to false
            $this->isParsed = false;
        }
        $this->parsedFile = $result;
    }

    public function getIsPreviewCreated()
    {
        if ($this->isParsed) {
            return $this->isPreviewCreated;
        }
    }

    public function getFileName()
    {
        if ($this->isParsed) {
            $path_parts = pathinfo($this->fileName);
            return $path_parts['basename'];
        }
    }

    public function getFileExtension()
    {
        if ($this->isParsed && !empty($this->fileName)) {
            $path_parts = pathinfo($this->fileName);
            return $path_parts['extension'];
        }
    }

    public function getTotalDistanceInKiloMeters()
    {
        if ($this->isParsed && $this->totalDistance > 0) {
            $km = floor($this->totalDistance / 1000);
            $rkm = $this->totalDistance % 1000;
            return $km . "." . $rkm;
        }
    }

    public function getTotalDuration()
    {
        if ($this->isParsed) {
            $time = $this->totalDuration;
            if ($time > 3600) {
                return $this->convertSecondsToHHMMSS($time);
            } else {
                return $this->convertSecondsToMMSS($time);
            }
        }
    }

    private function convertSecondsToHHMMSS($time)
    {
        return gmdate("H:i:s", $time);
    }

    private function convertSecondsToMMSS($time)
    {
        return gmdate("i:s", $time);
    }

    public function getAvaragePace()
    {
        if ($this->isParsed) {
            $dis_pace = $this->totalDistance / 1000;
            // getting seconds per km
            $pace = $this->totalDuration / $dis_pace;
            return $this->convertSecondsToMMSS($pace);
        } else {
            return 0;
        }
    }

    public function getBurnedCalories()
    {
        if ($this->isParsed) {
            $weight = $this->weight;
            $km = floor($this->totalDistance / 1000);
            return floor($km * $weight * 1.036);
        }
    }

    public function getMinElevation()
    {
        if ($this->isParsed && $this->minElevation < 10000) {
            return round($this->minElevation);
        }
    }

    public function getMaxElevation()
    {
        if ($this->isParsed && $this->maxElevation > 0) {
            return round($this->maxElevation);
        }
    }

    public function getNumberOfLaps()
    {
        if ($this->isParsed && isset($this->lapTimeSamples)) {
            $lapCounter = 0;
            foreach ($this->lapTimeSamples as $lapTimeSample) {
                $lapCounter++;
            }
            return $lapCounter;
        } else {
            return 0;
        }

    }

    public function getTimeOfParticularLap($lapNumber)
    {
        if ($this->isParsed && isset($this->lapTimeSamples)) {
            foreach ($this->lapTimeSamples as $lapTimeSample) {
                if ($lapTimeSample['lap'] == (int)$lapNumber) {
                    return $lapTimeSample['elapsedTime'];
                }
            }
        } else {
            return 0;
        }
    }

    public function getPercentageOfParticularLap($lapNumber)
    {
        if ($this->isParsed && isset($this->lapTimeSamples)) {
            $maxValue = 0;
            // Get the max value
            foreach ($this->lapTimeSamples as $lapTimeSample) {
                if ($lapTimeSample['percentage'] >= $maxValue) {
                    $maxValue = $lapTimeSample['percentage'];
                }
            }
            // Get percentage
            foreach ($this->lapTimeSamples as $lapTimeSample) {
                if ($lapTimeSample['lap'] == (int)$lapNumber) {
                    return 140 - floor(($lapTimeSample['percentage'] / $maxValue) * 100);
                }
            }
        } else {
            return 0;
        }
    }

    public function getLapTimeSamples()
    {
        return json_encode($this->lapTimeSamples);
    }

    public function getFilePath()
    {
        if ($this->isParsed) {
            return $this->filePath;
        }
    }

    public function getIsMapAvailable()
    {
        return $this->isMapAvailable;
    }

    public function getWorkoutType()
    {
        if ($this->isParsed) {
            return $this->workoutType;
        }
    }

    public function getBurnedCaloriesPerKilometer()
    {
        if ($this->isParsed) {
            $weight = $this->weight;
            return $weight * 1.036;
        }
    }

    public function getBurnedCaloriesPerHour()
    {
        if ($this->isParsed) {
            $weight = $this->weight;
            $km = floor($this->totalDistance / 1000);
            return ($km * $weight * 1.036) * (3600 / $this->getDurationInSeconds());
        }
    }

    public function getDurationInSeconds()
    {
        if ($this->isParsed) {
            return $this->totalDuration;
        }
    }

    public function getJSON()
    {
        if ($this->isParsed) {
            return $this->parsedFile;
        }
    }

    public function getMaxSpeed()
    {
        if ($this->isParsed && $this->maxSpeed > 0) {
            return round($this->maxSpeed);
        }
    }

    public function getMinSpeed()
    {
        if ($this->isParsed && $this->minSpeed < 10000) {
            return round($this->minSpeed);
        }
    }

    public function getMaxHeartRate()
    {
        if ($this->isParsed && $this->maxHeartRate > 0 && $this->isHeartRateExist) {
            return round($this->maxHeartRate);
        }
    }

    public function getMinHeartRate()
    {
        if ($this->isParsed && $this->minHeartRate < 10000 && $this->isHeartRateExist) {
            return round($this->minHeartRate);
        }
    }

    public function getMaxTemperature()
    {
        if ($this->isParsed && $this->maxTemperature > 0 && $this->isTemperatureExist) {
            return round($this->maxTemperature);
        }
    }

    public function getMinTemperature()
    {
        if ($this->isParsed && $this->minTemperature < 10000 && $this->isTemperatureExist) {
            return round($this->minTemperature);
        }
    }

    public function getMaxCadence()
    {
        if ($this->isParsed && $this->maxCadence < 10000 && $this->isCadenceExist) {
            return round($this->maxCadence);
        }
    }

    public function getMinCadence()
    {
        if ($this->isParsed && $this->minCadence < 10000 && $this->isCadenceExist) {
            return round($this->minCadence);
        }
    }

    public function getHeartRateZones($zone)
    {
        // TODO u olesne treba je to jen 86 procent ...
        if ($this->isHeartRateExist) {
            switch ($zone) {
                case 0:
                    // zone 0 is from 60 to 123
                    return ceil($this->getPercentageOfParticularHeartRateZone(0, 124));
                    break;
                case 1:
                    // zone 1 is from 124 to 135
                    return ceil($this->getPercentageOfParticularHeartRateZone(124, 135));
                    break;
                case 2:
                    // zone 2 is from 136 to 148
                    return ceil($this->getPercentageOfParticularHeartRateZone(135, 148));
                    break;
                case 3:
                    // zone 3 is from 149 to 161
                    return ceil($this->getPercentageOfParticularHeartRateZone(148, 161));
                    break;
                case 4:
                    // zone 4 is from 162 to 173
                    return ceil($this->getPercentageOfParticularHeartRateZone(161, 173));
                    break;
                case 5:
                    // zone 5 is from 174 to 187 - a MAX
                    return ceil($this->getPercentageOfParticularHeartRateZone(173, 250));
                    break;
            }
        }

        return 0;
    }

    private function getPercentageOfParticularHeartRateZone($fromHR, $toHR)
    {
        return ($this->getCountOfParticularHeartRateZone($fromHR, $toHR) / count($this->heartRateSamples)) * 100; // returns how many time user was in particular zone in PERCENT
    }

    private function getCountOfParticularHeartRateZone($fromHR, $toHR)
    {
        $counterZone = 0;
        foreach ($this->heartRateSamples as $heartRateSample) {
            if ($heartRateSample > $fromHR && $heartRateSample < $toHR) {
                $counterZone++;
            }
        }

        return $counterZone; // returns how many time user was in particular zone
    }

    public function getTotalDistanceInMeters()
    {
        if ($this->isParsed && $this->totalDistance > 0) {
            return round($this->totalDistance);
        }
    }
}
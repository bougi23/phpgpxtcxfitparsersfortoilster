<?php
session_start();
define('DB_SERVER', 'dev.toilster.com');
define('DB_USERNAME', 'XX');
define('DB_PASSWORD', 'XXX');
define('DB_DATABASE', 'XXX');
define("BASE_URL", "http://www.tititbe.com/");
//
define('DIR_BASE', $_SERVER['DOCUMENT_ROOT']);
// Debug mode // false when in PRODUCTION !!!
define('DEBUG', true);

function getDB()
{
    $dbhost = DB_SERVER;
    $dbuser = DB_USERNAME;
    $dbpass = DB_PASSWORD;
    $dbname = DB_DATABASE;
    $dbConnection = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbConnection->exec("set names utf8"); //utf-8 support
    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbConnection;
}
